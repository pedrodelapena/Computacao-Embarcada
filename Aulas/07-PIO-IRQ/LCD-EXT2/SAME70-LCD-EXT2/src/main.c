#include "asf.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "ioport.h"
#include "logo.h"

#define STRING_EOL    "\r\n"
#define STRING_HEADER "-- SAME70 LCD DEMO --"STRING_EOL	\
	"-- "BOARD_NAME " --"STRING_EOL	\
	"-- Compiled: "__DATE__ " "__TIME__ " --"STRING_EOL
	
#define LED_PIO_ID ID_PIOC
#define LED_PIO_PIN 30
#define LED_PIO_PIN_MASK (1 << LED_PIO_PIN)
#define LED_PIO PIOC

#define BUT_PIO_ID ID_PIOD
#define BUT_PIO_PIN 28
#define BUT_PIO_PIN_MASK (1 << BUT_PIO_PIN)
#define BUT_PIO PIOD

#define BUT_PIO_2_ID ID_PIOC
#define BUT_PIO_2_PIN 31
#define BUT_PIO_2_PIN_MASK (1 << BUT_PIO_2_PIN)
#define BUT_PIO_2 PIOC

struct ili9488_opt_t g_ili9488_display_opt;

/**
}
/**
 * \brief Configure UART console.
 */
static void configure_console(void)
{
	const usart_serial_options_t uart_serial_options = {
		.baudrate =		CONF_UART_BAUDRATE,
		.charlength =	CONF_UART_CHAR_LENGTH,
		.paritytype =	CONF_UART_PARITY,
		.stopbits =		CONF_UART_STOP_BITS,
	};

	/* Configure UART console. */
	sysclk_enable_peripheral_clock(CONSOLE_UART_ID);
	stdio_serial_init(CONF_UART, &uart_serial_options);
}


static void configure_lcd(void){
	/* Initialize display parameter */
	g_ili9488_display_opt.ul_width = ILI9488_LCD_WIDTH;
	g_ili9488_display_opt.ul_height = ILI9488_LCD_HEIGHT;
	g_ili9488_display_opt.foreground_color = COLOR_CONVERT(COLOR_WHITE);
	g_ili9488_display_opt.background_color = COLOR_CONVERT(COLOR_WHITE);

	/* Initialize LCD */
	ili9488_init(&g_ili9488_display_opt);
	ili9488_draw_filled_rectangle(0, 0, ILI9488_LCD_WIDTH-1, ILI9488_LCD_HEIGHT-1);
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_TOMATO));
	ili9488_draw_filled_rectangle(0, 0, ILI9488_LCD_WIDTH-1, 120-1);
	ili9488_draw_filled_rectangle(0, 360, ILI9488_LCD_WIDTH-1, 480-1);
	ili9488_draw_pixmap(0, 50, 319, 129, logoImage);
	
}

/**
 * \brief Main application function.
 *
 * Initialize system, UART console, network then start weather client.
 *
 * \return Program return value.
 */

volatile but_flag_l = false;
volatile but_flag_h = false;

void but_callBack_l(void){
	but_flag_l = true;
}

void but_callBack_h(void){
	but_flag_h = true;
}

void pisca_led(int freq){
	for(int counter = 0; counter <= 5; counter++){
		if (freq <= 0){
			freq = 0;
		}
		else {
			pio_set(LED_PIO, LED_PIO_PIN_MASK);
			delay_ms(freq/2);
			pio_clear(LED_PIO, LED_PIO_PIN_MASK);
			delay_ms(freq/2);
		}
	}
}
void write_freq(uint32_t freq){
	uint8_t stingLCD[256];
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_WHITE));
	ili9488_draw_filled_rectangle(10, 200, ILI9488_LCD_WIDTH-1, 215);
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_BLACK));
	sprintf(stingLCD, "Frequencia: %d Hz", freq);
	ili9488_draw_string(10, 200, stingLCD);
}



int main(void)
{
	// array para escrita no LCD
	uint8_t stingLCD[256];
	
	/* Initialize the board. */
	sysclk_init();
	board_init();
	ioport_init();
	
	
	/* Initialize the UART console. */
	configure_console();
	printf(STRING_HEADER);
	
	pio_configure(LED_PIO, PIO_OUTPUT_0, LED_PIO_PIN_MASK, PIO_DEFAULT);
		
	pio_enable_interrupt(BUT_PIO, BUT_PIO_PIN_MASK);
	pio_enable_interrupt(BUT_PIO_2, BUT_PIO_2_PIN_MASK);

	pio_handler_set(BUT_PIO, BUT_PIO_ID, BUT_PIO_PIN_MASK, BUT_PIO_PIN,  but_callBack_l);
	
	pio_handler_set(BUT_PIO_2, BUT_PIO_2_ID, BUT_PIO_2_PIN_MASK, BUT_PIO_2_PIN,  but_callBack_h);
	
	NVIC_EnableIRQ(BUT_PIO_ID);
	NVIC_SetPriority(BUT_PIO_ID, 0);
	
	NVIC_EnableIRQ(BUT_PIO_2_ID);
	NVIC_SetPriority(BUT_PIO_2_ID, 0);

    /* Inicializa e configura o LCD */
	configure_lcd();


    /* Escreve na tela Computacao Embarcada 2018 */
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_WHITE));
	ili9488_draw_filled_rectangle(0, 300, ILI9488_LCD_WIDTH-1, 315);
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_BLACK));
	
	sprintf(stingLCD, "Computacao Embarcada %d", 2018);
	ili9488_draw_string(10, 300, stingLCD);
	
	int delay = 50;

	while (1) {
		pmc_sleep(SAM_PM_SMODE_SLEEP_WFI);	
		//Diminui a frequencia do led caso o botao 1 seja precionado
		if (but_flag_l){
			delay -= 10;
			write_freq(delay);
			but_flag_l = false;
		}
		
		//Aumenta a frequencia do led caso o botao 2 seja precionado
		else if (but_flag_h){
			delay += 10;
			if (delay <= 0){
				delay = 0;
			}
			write_freq(delay);
			but_flag_h = false;
		}
		
	pisca_led(delay);
	}
	return 0;
}
