/**
 *	Avaliacao intermediaria 
 *	Computacao - Embarcada
 *        Abril - 2018
 * Objetivo : criar um Relogio + Timer 
 * Materiais :
 *    - SAME70-XPLD
 *    - OLED1
 *
 * Exemplo OLED1 por Eduardo Marossi
 * Modificacoes: 
 *    - Adicionado nova fonte com escala maior
 */

#include "asf.h"
#include "oled/gfx_mono_ug_2832hsweg04.h"
#include "oled/gfx_mono_text.h"
#include "oled/sysfont.h"

#define YEAR        2018
#define MOUNTH      3
#define DAY         19
#define WEEK        12
#define HOUR        15
#define MINUTE      45
#define SECOND      0

/**
* LEDs
*/

#define LED2_PIO PIOC
#define LED2_PIO_ID ID_PIOC
#define LED2_PIN 30
#define LED2_PIN_MASK (1 << LED2_PIN)

#define LED1_PIO PIOA
#define LED1_PIO_ID ID_PIOA
#define LED1_PIN 0
#define LED1_PIN_MASK (1 << LED1_PIN)

/**
* Botao
*/
#define BUT1_PIO PIOD
#define BUT1_PIO_ID ID_PIOD
#define BUT1_PIN 28
#define BUT1_PIN_MASK (1 << BUT1_PIN)
#define BUT1_DEBOUNCING_VALUE 79

/************************************************************************/
/* VAR globais                                                          */
/************************************************************************/

volatile uint8_t flag_alarm = 0;
volatile uint8_t flag_led1 = 0;
volatile uint8_t but_flag = 0;

//contador de minutos para o alarme
int counter = 0;

//deshabilita o alarme 
volatile uint32_t kill_alarm = 0; 

uint32_t lcdminute;

uint32_t hour, second, minute;

/************************************************************************/
/* PROTOTYPES                                                           */
/************************************************************************/

void BUT_init(void);
void LED_init(int estado);
void RTC_init(void);
void pin_toggle(Pio *pio, uint32_t mask);

/************************************************************************/
/* Handlers                                                             */
/************************************************************************/

/**
*  Handler Interrupcao botao 1
*/
static void Button1_Handler(uint32_t id, uint32_t mask){
	pin_toggle(LED2_PIO, LED2_PIN_MASK);
	counter += 1;
	but_flag = 1;
	
}


/**
* \brief Interrupt handler for the RTC. Refresh the display.
*/
void RTC_Handler(void)
{
	uint32_t ul_status = rtc_get_status(RTC);

	/*
	*  Verifica por qual motivo entrou
	*  na interrupcao, se foi por segundo
	*  ou Alarm
	*/
	if ((ul_status & RTC_SR_SEC) == RTC_SR_SEC) {
		rtc_clear_status(RTC, RTC_SCCR_SECCLR);
		rtc_get_time(RTC, &hour, &minute, &second);
		if (lcdminute != minute){
			lcdminute = minute;
			//gfx_mono_draw_filled_circle(115, 5, 5, GFX_PIXEL_SET, GFX_WHOLE);
			char current_time[40];
			sprintf(current_time, "%d:%d", hour, minute);
			gfx_mono_draw_string(current_time, 0, 0, &sysfont);
		}
	}
	
	/* Time or date alarm */
	if ((ul_status & RTC_SR_ALARM) == RTC_SR_ALARM) {
		rtc_clear_status(RTC, RTC_SCCR_ALRCLR);	
		
		if(hour = 23 && minute == 59){
			minute = -1;
			hour = 0;
		}

		if (minute = 59){
			minute = 0;
			hour += 1;
		}
		
		if (second > 59){
			minute = +1;
			second = 0;
		}
		
	
		if (counter != 0){
			counter --;
		}
		//****//
		if (counter == 0 && but_flag == 1) {
			flag_alarm = 1;
			but_flag = 0;
		}
		
		else {
			counter = 0;
		}
	}
	
	rtc_clear_status(RTC, RTC_SCCR_ACKCLR);
	rtc_clear_status(RTC, RTC_SCCR_TIMCLR);
	rtc_clear_status(RTC, RTC_SCCR_CALCLR);
	rtc_clear_status(RTC, RTC_SCCR_TDERRCLR);
	
}

/************************************************************************/
/* Funcoes                                                              */
/************************************************************************/

/**
*  Toggle pin controlado pelo PIO (out)
*/
void pin_toggle(Pio *pio, uint32_t mask){
	
	if(pio_get_output_data_status(pio, mask)){
		pio_clear(pio, mask);
	}
	else{
		pio_set(pio,mask);	
	}
	rtc_set_time_alarm(RTC, 1, hour, 1, minute+1, 1, second);
}

/**
* @Brief Inicializa o pino do BUT
*/
void BUT_init(void){
	pmc_enable_periph_clk(BUT1_PIO_ID);
	pio_set_input(BUT1_PIO, BUT1_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);
		
	pio_enable_interrupt(BUT1_PIO, BUT1_PIN_MASK);
	pio_handler_set(BUT1_PIO, BUT1_PIO_ID, BUT1_PIN_MASK, PIO_IT_FALL_EDGE, Button1_Handler);
		
	NVIC_EnableIRQ(BUT1_PIO_ID);
	NVIC_SetPriority(BUT1_PIO_ID, 1);
};

/**
* @Brief Inicializa o pino do LED
*/
void LED_init(int estado){
	
	pmc_enable_periph_clk(LED1_PIO_ID);
	pio_set_output(LED1_PIO, LED1_PIN_MASK, estado, 0, 0 );
	
	pmc_enable_periph_clk(LED2_PIO_ID);
	pio_set_output(LED2_PIO, LED2_PIN_MASK, estado, 0, 0 );
};

/**
* Configura o RTC para funcionar com interrupcao de alarme
*/
void RTC_init(){
	/* Configura o PMC */
	pmc_enable_periph_clk(ID_RTC);

	/* Default RTC configuration, 24-hour mode */
	rtc_set_hour_mode(RTC, 0);

	/* Configura data e hora manualmente */
	rtc_set_time(RTC, HOUR, MINUTE, SECOND);

	/* Configure RTC interrupts */
	NVIC_DisableIRQ(RTC_IRQn);
	NVIC_ClearPendingIRQ(RTC_IRQn);
	NVIC_SetPriority(RTC_IRQn, 0);
	NVIC_EnableIRQ(RTC_IRQn);

	/* Ativa interrupcao via alarme */
	rtc_enable_interrupt(RTC,  RTC_IER_ALREN | RTC_IER_SECEN); //INTERRUPCAO	

}

int main (void)
{
	board_init();
	sysclk_init();
	delay_init();
	gfx_mono_ssd1306_init();

	/* Disable the watchdog */
	WDT->WDT_MR = WDT_MR_WDDIS;

	/* Configura Leds */
	LED_init(0);

	/* Configura os botes */
	BUT_init();
  
	/** Configura RTC */
	RTC_init();
	
	/* configura alarme do RTC */
	//rtc_set_time_alarm(RTC, 1, hour, 1, minute, 1, second);

	flag_alarm = 0;
	counter = 0;
	but_flag = 0;
	
	pio_set(LED1_PIO, LED1_PIN_MASK);
	rtc_get_time(RTC, &hour, &minute, &second);
	char current_time[40];
	sprintf(current_time, "%d:%d", hour, minute);
	gfx_mono_draw_string(current_time, 0, 0, &sysfont);
	
	while(1) {
		
		if (flag_alarm) {
			for(int i = 0;i<=5;i++){
				pio_clear(LED1_PIO, LED1_PIN_MASK);
				delay_ms(35);
			
				pio_set(LED1_PIO, LED1_PIN_MASK);
				delay_ms(35);
			}
			flag_alarm = 0;
			pio_set(LED2_PIO, LED2_PIN_MASK);
		}
		
		
	}
}
